import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import './styles/styles.css'
import './styles/animate.css'
import 'bootstrap/dist/css/bootstrap.css';
import { Dashboard, Login, SignUp, Packages, Package } from './components/views'
import { Sidebar, TopNavbar, LoaderOverlay } from './components/containers'
import { Route, withRouter } from 'react-router-dom'
import 'semantic-ui-css/semantic.min.css'
import { connect } from 'react-redux'
import { getUser } from './actions/userActions'
import firebase from 'firebase'
import { NotVerifiedEmail, Message } from './components/messages'
import ReactGA from 'react-ga'

class App extends Component {

  constructor() {
    super()
    this.state = ({
      verified: false
    })
  }

  componentDidMount() {
    const { getUser, history } = this.props

    firebase.auth().onAuthStateChanged((user) => {
      if(user) {
        getUser(user.uid)
        this.setState({
          verified: user.emailVerified
        }, () => {
          if(this.props.location.pathname === '/') {
            history.push(`${process.env.PUBLIC_URL}/dashboard`)
          }
        })
      } else {
        if(this.props.location.pathname !== '/signup') {
          history.push(`${process.env.PUBLIC_URL}/login`)
        }
      }
    })

    ReactGA.initialize('UA-136343878-1')

    ReactGA.pageview(window.location.pathname + window.location.search)
    
  }

  didVerifyEmail = () => {
    window.location.reload();
  }

  render() {
    return (
      <div>

        <Message 
          visible={this.props.message.visible}
          text={this.props.message.text}
          type={this.props.message.type}
        />

        {this.props.loadingUser && this.props.location.pathname !== '/login' && this.props.location.pathname !== '/signup' &&
          <LoaderOverlay />
        }

        {this.props.location.pathname !== '/login' && this.props.location.pathname !== '/signup' && !this.state.verified &&
          <NotVerifiedEmail 
            didVerify={this.didVerifyEmail}
          />
        }

        {this.props.location.pathname !== '/login' && this.props.location.pathname !== '/signup' &&
        <Sidebar />
        }
        {this.props.location.pathname !== '/login' && this.props.location.pathname !== '/signup' &&
        <TopNavbar 
          route={this.props.route}
        />
        }

        {!this.props.loadingUser ?
        <div>
          <Route path="/dashboard" exact component={Dashboard} />
          <Route path="/login" exact component={Login} />
          <Route path="/signup" exact component={SignUp} />
          <Route path="/packages" exact component={Packages} />
          <Route path="/packages/edit/:id" exact component={Package} />
        </div>
        :
        <div>
          <Route path="/login" exact component={Login} />
          <Route path="/signup" exact component={SignUp} />
        </div>
        }

      </div>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user.user,
  loadingUser: state.user.loading,
  message: state.message,
  route: state.route.head
})

export default connect(mapStateToProps, { getUser })(App)
