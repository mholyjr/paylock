import React from 'react'
import moment from 'moment'

export function setFileIcon(ext) {
    if(ext.toLowerCase() === 'json' || ext.toLowerCase() === 'html' || ext.toLowerCase() === 'js' || ext.toLowerCase() === 'php' || ext.toLowerCase() === 'sql' || ext.toLowerCase() === 'css') {
        return (
            <i class="far fa-file-code"></i>
        )
    } else if (ext.toLowerCase() === 'pdf') {
        return (
            <i class="far fa-file-pdf red"></i>
        )
    } else if (ext.toLowerCase() === 'jpg' || ext.toLowerCase() === 'png' || ext.toLowerCase() === 'gif' || ext.toLowerCase() === 'svg' || ext.toLowerCase() === 'eps') {
        return (
            <i class="far fa-file-image"></i>
        )
    } else if (ext.toLowerCase() === 'mp4' || ext.toLowerCase() === 'mov' || ext === 'avi' || ext.toLowerCase() === 'wmv' || ext.toLowerCase() === 'flv') {
        return (
            <i class="far fa-file-video"></i>
        )
    }
}

export function humanFileSize(bytes, si) {
    var thresh = si ? 1000 : 1024;
    if(Math.abs(bytes) < thresh) {
        return bytes + ' B';
    }
    var units = si
        ? ['kB','MB','GB','TB','PB','EB','ZB','YB']
        : ['KiB','MiB','GiB','TiB','PiB','EiB','ZiB','YiB'];
    var u = -1;
    do {
        bytes /= thresh;
        ++u;
    } while(Math.abs(bytes) >= thresh && u < units.length - 1);

    return bytes.toFixed(1) + ' ' + units[u];
}

export function sumFileSize(key) {
    return this.reduce((a, b) => a + (b[key] || 0), 0);
}

export function setExpiresColor(date) {
    if(moment() > moment(date)) {
        return (
            <p className='expired-pkg'>{moment(date).format("D/M/Y")}</p>
        )
    } else {
        return (
            <p className='active-pkg'>{moment(date).format("D/M/Y")}</p>
        )
    }
}