import * as types from './types'

export function setMessage(type, text) {
    return (dispatch, getState) => {
        dispatch({type: types.SET_MESSAGE, msgType: type, text: text})

        setTimeout(() => {
            dispatch({type: types.UNSET_MESSAGE, msgType: '', text: ''})
        }, 2000);
    }
}