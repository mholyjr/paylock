import * as types from './types'

export function route(head) {
    return (dispatch, getState) => {
        dispatch({type: types.SET_ROUTE, route: head})
    }
}