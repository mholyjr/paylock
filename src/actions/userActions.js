import * as types from './types'

export function getUser(uid) {
    return (dispatch, getState) => {
        dispatch({type: types.FETCHING_USER, user: [], loading: true})

        return fetch('http://localhost:8888/paylock/user/get/?uid=' + uid)
        .then((data) => { return data.json() })
        .then((data) => {
            dispatch({type: types.USER_FETCHED, user: data.data, loading: false})
        })
    }
}