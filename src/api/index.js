import { api } from '../config'

export function createProfile(data) {
    return fetch('http://localhost:8888/paylock/user/create/', {
        method: 'POST',
        headers: 
        {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(data)
    })
    .then((response) => { return response.json()})
}

export function createPackage(data) {
    return fetch('http://localhost:8888/paylock/package/create/', {
        method: 'POST',
        headers: 
        {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(data)
    })
    .then((response) => { return response.json()})
}

export function getPackages(uid, limit) {
    return fetch(api + '/packages/get/?uid=' + uid + '&limit=' + limit)
    .then((data) => { return data.json() })
}

export function getLastPackages(uid) {
    return fetch(api + '/packages/get/last/?uid=' + uid)
    .then((data) => { return data.json() })
}

export function getPackagesFilter(uid, limit, by) {
    return fetch(api + '/packages/get/?uid=' + uid + '&limit=' + limit + '&filter=' + by)
    .then((data) => { return data.json() })
}

export function getPackage(id) {
    return fetch(api + '/package/get/?id=' + id)
    .then((data) => { return data.json() })
}

export function getPackageFiles(id) {
    return fetch(api + '/files/get/?id=' + id)
    .then((data) => { return data.json() })
}

export function deletePackage(id) {
    return fetch(api + '/package/delete/?id=' + id)
    .then((data) => { return data.json() })
}

export function addEmail(email, uid) {
    return fetch(api + '/user/email/create/?email=' + email + '&uid=' + uid)
    .then((data) => { return data.json() })
}

export function getEmailHistory(uid, pkg) {
    return fetch(api + '/user/email/get/?uid=' + uid + '&pkg=' + pkg)
    .then((data) => { return data.json() })
}

export function logSentEmails(data, pkg) {
    return fetch('http://localhost:8888/paylock/user/email/log/?pkg=' + pkg, {
        method: 'POST',
        headers: 
        {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(data)
    })
    .then((response) => { return response.json()})
}