import React from 'react'
import { Button } from 'semantic-ui-react'

const DeletePackageButton = ({ openModal }) => {
    return (
        <Button primary className='btn-delete' onClick={openModal}>Delete package</Button>
    )
}

export default DeletePackageButton