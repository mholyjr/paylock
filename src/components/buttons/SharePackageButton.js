import React from 'react'
import { Button } from 'semantic-ui-react'

const SharePackageButton = ({ openModal, id }) => {
    return (
        <Button primary className='btn-share' onClick={() => openModal()}>Share package</Button>
    )
}

export default SharePackageButton