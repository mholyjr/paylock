import React from 'react'

const EmailItem = ({ item, select }) => {
    return (
        <div className={!item.selected ? 'email-item-container non-selected' : 'email-item-container selected'}>
            <div className='row'>
                <div className='email-image-container col-sm-2 flex'>
                    {item.image === null ?
                        <div className='email-no-image'>
                            {item.email.charAt(0).toUpperCase()}
                        </div>
                    :
                        <div></div>
                    }
                </div>
                <div className='email-email-container col-sm-7 flex'>
                    <p className={!item.active && 'muted'}>{item.email}</p>
                </div>
                <div className='email-checkbox-container col-sm-3 flex flex-right'>
                    {item.active ?
                    <div className='email-checkbox' onClick={() => select(item.id)}>
                        <i className={!item.selected ? 'fas fa-plus' : 'fas fa-check'}></i>
                    </div>
                    :
                    <span className='email-sent shadow'>
                        Email sent
                    </span>
                    }
                </div>
            </div>
        </div>
    )
}

export default EmailItem