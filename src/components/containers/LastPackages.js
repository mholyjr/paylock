import React, { Component } from 'react'
import { getLastPackages } from '../../api'
import { LastPackageBig, LastPackageSmall } from '../widgets'
import { withRouter } from 'react-router-dom'

class LastPackages extends Component {

    constructor(props) {
        super(props)
        this.state = ({
            data: [],
            count: 0,
            loadingPackages: true,
            hasPackages: false
        })
    }

    componentDidMount() {
        getLastPackages(this.props.uid)
        .then((data) => {
            if(data.type === 'success') {
                this.setState({
                    data: data.data,
                    count: data.count,
                    loadingPackages: false,
                    hasPackages: true
                })
            } else {
                this.setState({
                    hasPackages: false
                })
            }
            
        })
    }

    render() {
        return (
            <div>
                {!this.state.loadingPackages &&
                <div className='row last-packages'>

                    <div className='col-sm-12 section-heading no-padding animated fadeInRight'>
                        <h2>Last packages</h2>
                    </div>

                    {this.state.count >= 1 &&
                    <div className='col-sm-6 no-padding-l'>
                        <LastPackageBig 
                            title={this.state.data[0].title}
                            message={this.state.data[0].message}
                            created={this.state.data[0].created}
                            expires={this.state.data[0].expires}
                            id={this.state.data[0].id_package}
                            price={this.state.data[0].price}
                            downloaded={this.state.data[0].downloaded}
                            history={this.props.history}
                        />
                    </div>
                    }

                    <div className='col-sm-6 no-padding-r'>

                        <div className='row row-lps'>
                            {this.state.count >= 2 &&
                            <div className='col-sm-6 no-padding-l'>
                                <LastPackageSmall
                                    title={this.state.data[1].title}
                                    message={this.state.data[1].message}
                                    created={this.state.data[1].created}
                                    expires={this.state.data[1].expires}
                                    id={this.state.data[1].id_package}
                                    history={this.props.history}
                                />
                            </div>
                            }

                            {this.state.count >= 3 &&
                            <div className='col-sm-6 no-padding-r'>
                                <LastPackageSmall
                                    title={this.state.data[2].title}
                                    message={this.state.data[2].message}
                                    created={this.state.data[2].created}
                                    expires={this.state.data[2].expires}
                                    id={this.state.data[2].id_package}
                                    history={this.props.history}
                                />
                            </div>
                            }
                        </div>

                        <div className='row row-lps'>
                            {this.state.count >= 4 &&
                            <div className='col-sm-6 no-padding-l'>
                                <LastPackageSmall
                                    title={this.state.data[3].title}
                                    message={this.state.data[3].message}
                                    created={this.state.data[3].created}
                                    expires={this.state.data[3].expires}
                                    id={this.state.data[3].id_package}
                                    history={this.props.history}
                                />
                            </div>
                            }

                            {this.state.count >= 5 &&
                            <div className='col-sm-6 no-padding-r'>
                                <LastPackageSmall
                                    title={this.state.data[4].title}
                                    message={this.state.data[4].message}
                                    created={this.state.data[4].created}
                                    expires={this.state.data[4].expires}
                                    id={this.state.data[4].id_package}
                                    history={this.props.history}
                                />
                            </div>
                            }
                        </div>

                    </div>
                </div>
                }
            </div>
        )
    }

}

export default withRouter(LastPackages)