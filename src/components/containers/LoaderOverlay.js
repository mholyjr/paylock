import React from 'react'
import { Dimmer, Loader } from 'semantic-ui-react'

const LoaderOverlay = () => {
    return (
        
        <Dimmer active>
            <Loader />
        </Dimmer>
    )
}

export default LoaderOverlay