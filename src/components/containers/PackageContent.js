import React from 'react'
import PackageContentItem from './PackageContentItem'
import { humanFileSize } from '../../actions/global'

const PackageContent = ({ files }) => {
    return (
        <div className='package-content-container shadow'>
            <div className='row package-content-head'>
                <div className='col-sm-1 ver-center'>
                    
                </div>
                <div className='col-sm-7 ver-center file-name-col'>
                    <p>Name</p>
                </div>
                <div className='col-sm-2 ver-center'>
                    <p>Type</p>
                </div>
                <div className='col-sm-2 ver-center'>
                    <p>Size</p>
                </div>
            </div>
            {
                files.map((file) => {
                    return (
                        <PackageContentItem file={file} />
                    )
                })
            }
            <div className='row package-content-footer'>
                <div className='col-sm-3 ver-center'>

                </div>
                <div className='col-sm-5 ver-center file-name-col'>

                </div>
                <div className='col-sm-2 ver-center'>
                    <p>Total size</p>
                </div>
                <div className='col-sm-2 ver-center'>
                    <p>{humanFileSize(files.reduce((a, b) => Number(a) + (Number(b['file_size']) || 0), 0), true)}</p>
                </div>
            </div>
        </div>
    )
}

export default PackageContent