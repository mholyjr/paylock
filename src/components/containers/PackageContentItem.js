import React from 'react'
import { setFileIcon, humanFileSize } from '../../actions/global'

const PackageContentItem = ({ file }) => {
    return (
        <div>
            <div className='row package-content-item'>
                <div className='col-sm-1 ver-center'>
                    {setFileIcon(file.file_name.split('.').pop())}
                </div>
                <div className='col-sm-7 ver-center file-name-col'>
                    <p>{file.file_name.replace(/\.[^/.]+$/, "")}</p>
                </div>
                <div className='col-sm-2 ver-center'>
                    {'.' + file.file_name.split('.').pop()}
                </div>
                <div className='col-sm-2 ver-center'>
                    {humanFileSize(file.file_size, true)}
                </div>
            </div>
        </div>
    )
}

export default PackageContentItem