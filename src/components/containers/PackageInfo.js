import React from 'react'
import moment from 'moment'

const PackageInfo = ({ pkg }) => {
    return (
        <div className='package-info-container shadow'>
            <div className='row row-info'>
                <label>Name</label>
                <p className='oneline'>{pkg.title}</p>
            </div>
            <div className='row row-info'>
                <div className='col-sm-6 no-padding-l'>
                    <label># of downloads</label>
                    <p>{pkg.downloaded}</p>
                </div>
                <div className='col-sm-6 no-padding-r'>
                    <label>Price</label>
                    <p>{'€' + pkg.price}</p>
                </div>
            </div>
            <div className='row row-info'>
                <div className='col-sm-6 no-padding-l'>
                    <label>Created</label>
                    <p>{moment(pkg.created).format("D/M/Y")}</p>
                </div>
                <div className='col-sm-6 no-padding-r'>
                    <label>Expires</label>
                    <p>{moment(pkg.expires).format("D/M/Y")}</p>
                </div>
            </div>
        </div>
    )
}

export default PackageInfo