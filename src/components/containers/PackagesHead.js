import React from 'react'

const PackagesHead = () => {
    return (
        <div className='packages-head'>
            <div className='row'>
                <div className='col-sm-1 package-id'>
                    <p>#</p>
                </div>
                <div className='col-sm-6 package-title'>
                    <p>Name</p>
                </div>
                <div className='col-sm-2 package-title'>
                    <p>Created</p>
                </div>
                <div className='col-sm-2 package-title'>
                    <p>Expires</p>
                </div>
                <div className='col-sm-1 package-title no-padding-l'>
                    <p>Details</p>
                </div>
            </div>
        </div>
    )
}

export default PackagesHead