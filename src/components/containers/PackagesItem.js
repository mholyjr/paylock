import React, { Component } from 'react'
import moment from 'moment'
import { setExpiresColor } from '../../actions/global'
import { Dropdown } from 'semantic-ui-react'
import { deletePackage } from '../../api'

class PackagesItem extends Component {

    render() {

        const { item, deletePkg, beforeDelete } = this.props

        return (
            <div className='packages-item fadeInUp animated'>
                <div className='row'>
                    <div className='col-sm-1 package-id'>
                        <p>{item.id}</p>
                    </div>
                    <div className='col-sm-6 package-title'>
                        <p>{item.title}</p>
                        <p className='item-message text-muted'>{item.message}</p>
                    </div>
                    <div className='col-sm-2 package-created'>
                        <p>{moment(item.created).format("D/M/Y")}</p>
                    </div>
                    <div className='col-sm-2 package-expires'>
                        <p>{setExpiresColor(item.expires)}</p>
                    </div>
                    <div className='col-sm-1 package-details no-padding-l'>
                        <Dropdown text='More' pointing className='link item'>
                            <Dropdown.Menu>
                                <Dropdown.Header>More</Dropdown.Header>
                                <Dropdown.Item><a href={'/packages/edit/' + item.id_package}>Details</a></Dropdown.Item>
                                <Dropdown.Divider />
                                <Dropdown.Item onClick={() => beforeDelete(item.id_package)}>Delete</Dropdown.Item>
                            </Dropdown.Menu>
                        </Dropdown>
                    </div>
                </div>
            </div>
        )
    }
}

export default PackagesItem