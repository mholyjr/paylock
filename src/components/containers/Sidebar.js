import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'

class Sidebar extends Component {

    render() {
        return (
            <div className='sidebar'>
                <div className='logo-container'>
                    <p className='logo'>Paylock</p>
                </div>
                <div className='nav-container'>
                    <nav className='sidebar-nav'>
                        <ul>
                            <li><NavLink to={'/dashboard'} className='sidebar-new-calendar-link' activeClassName="active">Dashboard</NavLink></li>
                            <li><NavLink to={'/packages'} className='sidebar-new-calendar-link' activeClassName="active">My packages</NavLink></li>
                            <li><a href='#'>My profile</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        )
    }

}

export default Sidebar