import React from 'react'
import { Button } from 'semantic-ui-react'

const SubscriptionDetails = ({ name, details }) => {
    return (
        <div className='gradient-block'>
            <div className='block-head'>
                <div className='row'>
                    <div className='col-sm-10 block-heading'>
                        <h3>{name}</h3>
                    </div>
                </div>
            </div>
            <div className='subscription-details'>
                {details.map((item) => {
                    return (
                        <div className='row'>
                            <div className='icon-wrapper'>
                                <i className={item.icon}></i>
                            </div>
                            <div className='details-text'>
                                <p>{item.text}</p>
                            </div>
                        </div>
                    )
                })}

                <div className='row'>
                    <div className='col-sm-12 button'>
                        <Button primary className='green'>Upgrade account</Button>
                    </div>
                </div>

            </div>
        </div>
    )
}

export default SubscriptionDetails