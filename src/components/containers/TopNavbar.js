import React, { Component } from 'react'
import { Input, Menu } from 'semantic-ui-react'
import firebase from 'firebase'
import { connect } from 'react-redux'

class TopNavbar extends Component {

    handleItemClick = (e, { name }) => this.setState({ activeItem: name })

    handleLogout = () => {
        firebase.auth().signOut()
    }

    render() {

        const { history, route } = this.props

        return (
            <Menu secondary fixed='top'>
                <Menu.Menu position='left'>
                    <Menu.Item>
                        {!this.props.userLoading &&
                        <p className='navbar-route'>{route}</p>
                        }
                    </Menu.Item>
                </Menu.Menu>
                <Menu.Menu position='right'>
                    <Menu.Item>
                        {!this.props.userLoading &&
                        <p className='navbar-greeting'>Hello, {this.props.user.first_name}</p>
                        }
                    </Menu.Item>
                    <Menu.Item
                        name='logout'
                        onClick={this.handleLogout}
                    />
                </Menu.Menu>
          </Menu>
        )
    }
}
    
const mapStateToProps = state => ({
    user: state.user.user[0],
    userLoading: state.user.loading
})
  
export default connect(mapStateToProps, {})(TopNavbar)