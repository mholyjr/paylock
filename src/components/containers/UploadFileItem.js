import React from 'react'
import { setFileIcon, humanFileSize } from '../../actions/global'

const UploadFileItem = ({ file }) => {
    return (
        <div>
            <div className='row dnd-file-item'>
                <div className='col-sm-1 ver-center'>
                    {setFileIcon(file.name.split('.').pop())}
                </div>
                <div className='col-sm-7 ver-center file-name-col'>
                    <p>{file.name.replace(/\.[^/.]+$/, "")}</p>
                </div>
                <div className='col-sm-1 ver-center'>
                    {'.' + file.name.split('.').pop()}
                </div>
                <div className='col-sm-2 ver-center'>
                    {humanFileSize(file.size, true)}
                </div>
                <div className='col-sm-1 ver-center'>
                    <i class="fas fa-times"></i>
                </div>
            </div>
        </div>
    )
}

export default UploadFileItem