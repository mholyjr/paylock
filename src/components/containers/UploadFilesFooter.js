import React from 'react'
import { setFileIcon, humanFileSize, sumFileSize } from '../../actions/global'

const UploadFilesFooter = ({ open, total }) => {
    return (
        <div>
            <div className='row dnd-file-item dnd-files-footer'>
                <div className='col-sm-8 ver-center'>
                    <p>Drag & Drop or <span className='spanlike-btn spanlike-btn-ghost-b' onClick={open}>Select files</span></p>
                </div>
                <div className='col-sm-1 ver-center'>
                    <p>Total</p>
                </div>
                <div className='col-sm-2 ver-center'>
                    <p>{humanFileSize(total.reduce((a, b) => a + (b['size'] || 0), 0), true)}</p>
                </div>
                <div className='col-sm-1 ver-center'>
                    
                </div>
            </div>
        </div>
    )
}

export default UploadFilesFooter