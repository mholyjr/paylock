import React from 'react'
import { setFileIcon, humanFileSize } from '../../actions/global'

const UploadFilesHead = () => {
    return (
        <div>
            <div className='row dnd-file-item dnd-files-head'>
                <div className='col-sm-1 ver-center'>
                    
                </div>
                <div className='col-sm-7 ver-center'>
                    <p>File name</p>
                </div>
                <div className='col-sm-1 ver-center'>
                    <p>Type</p>
                </div>
                <div className='col-sm-2 ver-center'>
                    <p>Size</p>
                </div>
                <div className='col-sm-1 ver-center'>
                    
                </div>
            </div>
        </div>
    )
}

export default UploadFilesHead