import React from 'react'
import { Button, Form } from 'semantic-ui-react'

const LoginForm = ({ onChange, onSubmit, loading }) => {

    return (
        <Form className='login-form' loading={loading} onSubmit={onSubmit} >

            <div className="form-head">
                <span>Login to enter</span>
            </div>

            <Form.Field>
                <label for='email'>E-mail</label>
                <input type='text' name='email' id='email' placeholder='E-mail' onChange={onChange} />
            </Form.Field>

            <Form.Field>
                <label for='password'>Password</label>
                <input type='password' name='password' id='password' placeholder='Password' onChange={onChange} />
            </Form.Field>

            <Form.Field className='submit-button'>
                <Button primary>Login</Button>
            </Form.Field>

        </Form>
    )

}

export default LoginForm