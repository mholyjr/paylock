import React from 'react'
import { Form } from 'semantic-ui-react'

const NewEmailForm = ({ onChange, submit }) => {
    return (
        <Form onSubmit={submit}>
            <Form.Field>
                <input type='email' name='email' id='email' placeholder='Add email and hit enter' onChange={onChange} />
            </Form.Field>
        </Form>
    )
}

export default NewEmailForm