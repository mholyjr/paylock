import React, { Component } from 'react'
import { Form, Button, TextArea, Select } from 'semantic-ui-react'
import Dropzone from 'react-dropzone'
import { UploadFileItem, UploadFilesHead, UploadFilesFooter } from '../containers'

const deleteOptions = [
                        { key: '1', value: '1', text: 'Only once' },
                        { key: '2', value: '2', text: 'Three times' },
                        { key: '3', value: '3', text: 'Five times' },
                        { key: '4', value: '4', text: 'Ten times' },
                        { key: '5', value: '5', text: 'Unlimited', disabled: true }
                    ]

class NewPackageForm extends Component {

    constructor(props) {
        super(props)
        this.state = ({
            files: [],
            totalSize: ''
        })
    }

    onDrop(files) {
        this.setState({
            files: this.state.files.concat(files)
        }, () => {
            console.log(this.state.files)
        })
    }

    componentDidUpdate(prevProps) {
        // Typical usage (don't forget to compare props):
        if (this.props.resetFiles !== prevProps.resetFiles && this.props.resetFiles === true) {
            this.setState({
                files: [],
                totalSize: ''
            })
        }
    }

    render() {

        const { loading, onSubmit, reset, onChange, price, message, onChangeSelect, deleteTime, title } = this.props

        return (
            <div className='new-package-form'>
                <Form enctype='multipart/form-data' loading={loading}>
    
                    <Dropzone onDrop={this.onDrop.bind(this)} disabledClick>
                        {({getRootProps, getInputProps, open, sum}) => (
                            <section>
                                <div {...getRootProps(
                                    {
                                        onClick: event => event.stopPropagation(),
                                        onKeyDown: event => {
                                          if (event.keyCode === 32 || event.keyCode === 13) {
                                            event.stopPropagation()
                                          }
                                        }
                                      }
                                )} className={this.state.files.length === 0 ? 'dnd-container-empty' : 'dnd-container-hasfiles'}>
                                    <input {...getInputProps()} />
                                    {this.state.files.length === 0 &&
                                    <p>
                                        Drag & Drop files here <br />
                                        or<br /> 
                                        <span className='spanlike-btn spanlike-btn-ghost-b' onClick={open}>Select files</span>
                                    </p>
                                    }

                                    {this.state.files.length !== 0 &&
                                        <div>
                                        <UploadFilesHead />
                                        {this.state.files.map((item) => {
                                            return (
                                                <UploadFileItem file={item} />
                                            )
                                        })}
                                        <UploadFilesFooter 
                                            open={open}
                                            total={this.state.files}
                                        />
                                        </div>
                                    }

                                </div>
                            </section>
                        )}
                    </Dropzone>
    
                    <div className='col-sm-12 additional-inputs'>
                        <div className='row'>
                            <Form.Field>
                                <label for='title'>Package name</label>
                                <input type='text' name='title' id='title' placeholder='Name' onChange={onChange} value={title} />
                            </Form.Field>
                        </div>
                        <div className='row'>
                            <div className='col-sm-6 no-padding-l'>
                                <Form.Field>
        
                                    <div className='input-50'>
                                        <label for='price'>Price</label>
                                        <input type='number' name='price' id='price' placeholder='Price' onChange={onChange} value={price} />
                                    </div>
            
                                    <div className='input-50'>
                                        <label for='you_get'><span className='text-muted'>You get</span></label>
                                        <input type='text' disabled name='you_get' id='you_get' value={'€' + (price - (price * 0.05))} />
                                    </div>
            
                                </Form.Field>
                            </div>

                            <div className='col-sm-6 no-padding-r'>
                                <Form.Field>
                                    <label for='delete_after'>Delete after <span className='text-muted'># of downloads</span></label>
                                    <Select placeholder='Select number of downloads' name='delete_after' id='delete_after' options={deleteOptions} value={deleteTime} onChange={onChangeSelect} />
                                </Form.Field>
                            </div>
                        </div>
    
                        <div className='row'>
                            <Form.Field>
                                <label for='message'>Message</label>
                                <TextArea name='message' id='message' placeholder='Message...' rows={8} onChange={onChange} value={message} />
                            </Form.Field>
                        </div>

                        <div className='row'>
                            <div className='col-sm-12 button'>
                                <Button primary className='primary-transparent-ghost' onClick={reset}>Reset</Button>
                                <Button primary className='green' disabled={price === '' && true} onClick={() => onSubmit(this.state.files)}>Save package</Button>
                            </div>
                        </div>

                    </div>
                    
                </Form>
            </div>
        )
    }

}
    

export default NewPackageForm