import React, { Component } from 'react'
import { NewEmailForm } from '../forms'
import { EmailItem } from '../containers'
import { addEmail, getEmailHistory, logSentEmails } from '../../api'
import { Button } from 'semantic-ui-react'
import { connect } from 'react-redux'
import { setMessage } from '../../actions/messageActions'

class ShareToEmail extends Component {

    constructor(props) {
        super(props)
        this.state = ({
            addEmail: '',
            emailHistory: [
                {
                    id: 0,
                    email: 'm.holyjr@gmail.com',
                    active: true,
                    image: null,
                    selected: false
                },
                {
                    id: 1,
                    email: 'info@web7master.com',
                    active: true,
                    image: null,
                    selected: false
                },
                {
                    id: 2,
                    email: 'info@web7master.com',
                    active: false,
                    image: null,
                    selected: false
                },
                {
                    id: 3,
                    email: 'info@web7master.com',
                    active: false,
                    image: null,
                    selected: false
                }
            ],
            selectedEmails: {
                sender: '',
                data: []
            }
        })
    }

    componentDidMount() {
        this.getEmailHistoryHandler()
    }

    getEmailHistoryHandler = () => {
        getEmailHistory(this.props.user.uid, this.props.packageId)
        .then((data) => {
            this.setState({
                emailHistory: data.data
            })
        })
    }

    renderEmailHistory = (emails) => {
        return (
            emails.map((item) => {
                return (
                    <EmailItem 
                        item={item} 
                        select={this.selectEmail}
                    />
                )
            })
        )
    }

    selectEmail = (id) => {
        this.setState({
            emailHistory: this.state.emailHistory.map(item => (item.id === id ? {...item, selected: !item.selected} : item))
        }, () => {
            this.setSelectedEmails()
        });
    }

    setSelectedEmails = () => {
        this.setState({
            selectedEmails: {
                sender: this.props.user.first_name,
                message: this.props.pkgMessage,
                data: this.state.emailHistory.filter(item => (item.selected && item))
            }
        })
    }

    onChangeInput = e => this.setState({
        addEmail: e.target.value
    })

    submitEmail = () => {
        addEmail(this.state.addEmail, this.props.user.uid)
        .then((res) => {
            this.getEmailHistoryHandler()
        })
    }

    sendEmails = () => {
        logSentEmails(this.state.selectedEmails, this.props.packageId)
        .then((res) => {
            this.props.hideModal()
            this.props.setMessage(res.type, res.text)
        })
    }

    render() {
        return (
            <div>
                <NewEmailForm 
                    onChange={this.onChangeInput} 
                    submit={this.submitEmail}
                />

                {this.state.emailHistory.length === 0 ?
                    <div>
                        <p>You have an empty email history</p>
                        <p>Please add email above</p>
                    </div>
                    :
                <div className='emails-container'>
                    {this.renderEmailHistory(this.state.emailHistory)}
                </div>
                }

                <div className='flex-right no-padding-r modal-footer'>
                    <span>{'Selected: ' + this.state.selectedEmails.data.length}</span>
                    <Button primary className='green' onClick={this.sendEmails}>Send package</Button>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    user: state.user.user[0],
    userLoading: state.user.loading
})
  
export default connect(mapStateToProps, { setMessage })(ShareToEmail)