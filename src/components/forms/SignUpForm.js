import React from 'react'
import { Button, Form } from 'semantic-ui-react'

const SignUpForm = ({ onChange, loading, onSubmit }) => {

    return (
        <Form className='login-form' loading={loading} onSubmit={onSubmit}>

            <div class="form-head">
                <span>Sign up to Paylock</span>
            </div>

            <Form.Field>
                <label for='first_name'>First name</label>
                <input type='text' name='first_name' id='first_name' placeholder='First name' onChange={onChange} />
            </Form.Field>

            <Form.Field>
                <label for='last_name'>Last name</label>
                <input type='text' name='last_name' id='last_name' placeholder='Last name' onChange={onChange} />
            </Form.Field>

            <Form.Field>
                <label for='email'>E-mail</label>
                <input type='text' name='email' id='email' placeholder='E-mail' onChange={onChange} />
            </Form.Field>

            <Form.Field>
                <label for='password'>Password</label>
                <input type='password' name='password' id='password' placeholder='Password' onChange={onChange} />
            </Form.Field>

            <Form.Field className='submit-button'>
                <Button primary>Create account</Button>
            </Form.Field>

        </Form>
    )

}

export default SignUpForm