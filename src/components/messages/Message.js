import React from 'react'
import { Button } from 'semantic-ui-react'

const Message = ({ text, type, visible }) => {
    return (
        <div className={visible ? 'message-container' : 'message-container hidden'} style={type === 'success' ? { backgroundColor: '#42C3AF' } : { backgroundColor: '#FF4653' }}>
            <div className='row'>
                <div className='col-sm-8 no-padding-l ver-center'>
                    <p>{text}</p>
                </div>
            </div>
        </div>
    )
}

export default Message