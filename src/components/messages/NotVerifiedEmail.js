import React from 'react'
import { Button } from 'semantic-ui-react'

const NotVerifiedEmail = ({ didVerify }) => {
    return (
        <div className='not-verified-container'>
            <div className='row'>
                <div className='col-sm-8 no-padding-l ver-center'>
                    <p>Please verify your email. You won't be able to send a package before verification.</p>
                </div>
                <div className='col-sm-4 col-right no-padding'>
                    <Button secondary onClick={didVerify}>I did</Button>
                    <Button secondary className='btn-ghost-red'>Re-send</Button>
                </div>
            </div>
        </div>
    )
}

export default NotVerifiedEmail