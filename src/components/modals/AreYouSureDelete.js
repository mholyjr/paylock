import React, { Component } from 'react'
import { Button } from 'semantic-ui-react'
import { connect } from 'react-redux'
import { setMessage } from '../../actions/messageActions'
import { deletePackage } from '../../api';

class AreYouSureDelete extends Component {

    constructor(props) {
        super(props)
        this.state = ({
            isClosing: false
        })
    }

    componentDidUpdate(prevProps) {

        const { isOpen } = this.props

        if (isOpen !== prevProps.isOpen && isOpen === false) {
            this.setState({
                isClosing: true
            }, () => {
                setTimeout(() => {
                    this.setState({
                        isClosing: false
                    })
                }, 1500
                );
            })
        }
    }

    setClass = (state) => {
        if(state) {
            return 'drawer-modal open'
        } else if(!state && this.state.isClosing) {
            return 'drawer-modal closing'
        } else if(!state && !this.state.isClosing) {
            return 'drawer-modal closed'
        }
    }

    setClassDimmer = (state) => {
        if(state) {
            return 'drawer-modal-dimmer red open'
        } else if(!state && this.state.isClosing) {
            return 'drawer-modal-dimmer red closing'
        } else if(!state && !this.state.isClosing) {
            return 'drawer-modal-dimmer red closed'
        }
    }

    setClassHead = (state) => {
        if(state) {
            return 'animated fadeInLeft'
        } else if(!state && this.state.isClosing) {
            return 'animated fadeOutLeft'
        }
    }

    render() {

        const { id, message, isOpen, hideModal, deletePkg } = this.props

        return (
            <div>
                <div className={this.setClass(isOpen)}>

                    <div className='modal-head'>
                        <h2 className={this.setClassHead(isOpen)}>{message}</h2>
                        <p>Are you sure you want to delete this package? If you do so, it's irreversible.</p>
                    </div>

                    <div className='modal-body'>
                        <Button primary className='btn-back' onClick={hideModal}>No, I want to go back</Button>
                        <Button primary className='btn-delete' onClick={() => deletePkg(id)}>Yes, I'm 100% sure</Button>
                    </div>

                    <div className='hide-modal' onClick={hideModal}>
                        <i className="far fa-times-circle"></i>
                    </div>

                </div>
                <div className={this.setClassDimmer(isOpen)}></div>
            </div>
        )

    }
}

const mapStateToProps = state => ({
    user: state.user.user[0],
    userLoading: state.user.loading
})
  
export default connect(mapStateToProps, { setMessage })(AreYouSureDelete)