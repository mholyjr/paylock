import React, { Component } from 'react'
import { Button, Tab } from 'semantic-ui-react'
import { ShareToEmail } from '../forms'
import { connect } from 'react-redux'
import { setMessage } from '../../actions/messageActions'

const panes = [
    { menuItem: 'Email', render: () => <ShareToEmail /> },
    { menuItem: 'Messanger', render: () => <Tab.Pane attached={false}>Tab 2 Content</Tab.Pane> },
    { menuItem: 'Embed', render: () => <Tab.Pane attached={false}>Tab 3 Content</Tab.Pane> },
]

class ShareModal extends Component {

    constructor(props) {
        super(props)
        this.state = ({
            isClosing: false
        })
    }

    componentDidUpdate(prevProps) {

        const { isOpen } = this.props

        if (isOpen !== prevProps.isOpen && isOpen === false) {
            this.setState({
                isClosing: true
            }, () => {
                setTimeout(() => {
                    this.setState({
                        isClosing: false
                    })
                }, 1500
                );
            })
        }
    }

    setClass = (state) => {
        if(state) {
            return 'drawer-modal open'
        } else if(!state && this.state.isClosing) {
            return 'drawer-modal closing'
        } else if(!state && !this.state.isClosing) {
            return 'drawer-modal closed'
        }
    }

    setClassDimmer = (state) => {
        if(state) {
            return 'drawer-modal-dimmer open'
        } else if(!state && this.state.isClosing) {
            return 'drawer-modal-dimmer closing'
        } else if(!state && !this.state.isClosing) {
            return 'drawer-modal-dimmer closed'
        }
    }

    setClassHead = (state) => {
        if(state) {
            return 'animated fadeInLeft'
        } else if(!state && this.state.isClosing) {
            return 'animated fadeOutLeft'
        }
    }

    render() {

        const { id, message, isOpen, hideModal } = this.props

        return (
            <div>
                <div className={this.setClass(isOpen)}>

                    <div className='modal-head'>
                        <h2 className={this.setClassHead(isOpen)}>{message}</h2>
                    </div>

                    <div className='modal-body'>
                        <Tab menu={{ secondary: true, pointing: true }} panes={panes} className={isOpen ? 'animated fadeInDown' : ''} />
                    </div>

                    <div className='hide-modal' onClick={hideModal}>
                        <i className="far fa-times-circle"></i>
                    </div>

                </div>
                <div className={this.setClassDimmer(isOpen)}></div>
            </div>
        )

    }
}

const mapStateToProps = state => ({
    user: state.user.user[0],
    userLoading: state.user.loading
})
  
export default connect(mapStateToProps, { setMessage })(ShareModal)