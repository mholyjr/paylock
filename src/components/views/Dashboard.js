import React, { Component } from 'react'
import { NewPackageForm } from '../forms'
import { Button } from 'semantic-ui-react'
import { SubscriptionDetails, LastPackages } from '../containers'
import { connect } from 'react-redux'
import { createPackage } from '../../api'
import axios from 'axios'
import { setMessage } from '../../actions/messageActions'
import { route } from '../../actions/routeActions'
import { ShareModal } from '../modals'
import { UpgradeAccount, AccountBalance, LastPackage, LastPackageBig, LastPackageSmall } from '../widgets'

class Dashboard extends Component {

    constructor(props) {
        super(props)
        this.state = ({
            route: 'Dashboard',
            data: {
                idPackage: '',
                uid: '',
                price: '',
                title: '',
                message: '',
                deleteTime: ''
            },
            loadingForm: false,
            resetFiles: false,
            openModal: false,
            freeAccount: [
                {
                    'icon' : 'fas fa-weight-hanging', 
                    'text' : 'Max size: 250MB'
                },
                {
                    'icon' : 'fas fa-copy', 
                    'text' : 'Max number of files: 10'
                },
                {
                    'icon' : 'far fa-clock', 
                    'text' : 'Deletes after: 5 days'
                },
                {
                    'icon' : 'fas fa-code', 
                    'text' : 'Embeed code is not available'
                },
                {
                    'icon' : 'fas fa-times', 
                    'text' : 'Limited number of downloads'
                }
            ]
        })
    }

    componentDidMount() {
        if(!this.props.userLoading) {
            this.setState({
                data: {
                    ...this.state.data,
                    idPackage: Date.now() + this.props.user.id_user,
                    uid: this.props.user.uid
                }
            })
        }

        this.props.route(this.state.route)
        
    }

    onChangeInput = e => this.setState({
        data: { ...this.state.data, [e.target.name]: e.target.value }
    })

    onChangeSelect = (event, data) => {
        this.setState({
            data: {
                ...this.state.data,
                deleteTime: data.value
            }
        })
    }

    reset = () => {
        this.setState({
            loadingForm: true,
            resetFiles: true,
            data: {
                price: '',
                message: '',
                deleteTime: '',
                title: ''
            }
        }, () => {
            this.setState({
                loadingForm: false,
                resetFiles: false,
            })
        })
    }

    submit = (files) => {
        if(this.state.data.idPackage !== '' && this.state.data.price !== '' && this.state.data.deleteTime !== '', this.state.data.message !== '') {
            this.setState({
                data: {
                    ...this.state.data,
                    idPackage: Date.now() + this.props.user.id_user,
                    uid: this.props.user.uid,
                    loadingForm: true
                }
            }, () => {
                createPackage(this.state.data)
                .then((res) => {
                    this.uploadFiles(files)
                })

            })
            
        } else {
            alert('Form is not filled')
        }
    }

    uploadFiles = (files) => {

        this.setState({
			loadingForm: true,
        })
        
        const fd = new FormData();
        files.forEach(file => {
            fd.append('packageFiles[]', file, file.name);
        })

        axios.post('http://localhost:8888/paylock/package/upload/?id=' + this.state.data.idPackage + '&uid=' + this.state.data.uid, fd)
        .then(res => {

            this.setState({
                loadingForm: false,
            })

            this.props.setMessage(res.data.type, res.data.text)

            if(res.data.type === 'success') {
                this.setState({
                    openModal: true
                })
                //this.reset()
            }

        })

    }
    
    hideModal = () => {
        this.setState({
            openModal: false
        }, () => {
            this.reset()
        })
    }

    render() {
        return (
            <div className='inner-container'>
                <div className='row widget-row animated fadeInDown'>
                    <div className='col-sm-4 no-padding-l'>
                        <LastPackage />
                    </div>
                    <div className='col-sm-4 no-padding-l'>
                        <AccountBalance />
                    </div>
                    <div className='col-sm-4 no-padding'>
                        <UpgradeAccount />
                    </div>
                </div>

                {/*<div className='row'>
                    <div className='col-sm-8 no-padding-l'>
                        <NewPackageForm 
                            onChange={this.onChangeInput}
                            onChangeSelect={this.onChangeSelect}
                            onSubmit={this.submit}
                            price={this.state.data.price}
                            title={this.state.data.title}
                            message={this.state.data.message}
                            reset={this.reset}
                            deleteTime={this.state.data.deleteTime}
                            loading={this.state.loadingForm}
                            resetFiles={this.state.resetFiles}
                        />
                    </div>
                    <div className='col-sm-4 no-padding'>
                        <SubscriptionDetails 
                            name='Free account'
                            details={this.state.freeAccount}
                        />
                    </div>
                </div>*/}

                <LastPackages 
                    uid={this.props.user.uid}
                />

                <ShareModal 
                    message='Share your package'
                    isOpen={this.state.openModal}
                    hideModal={this.hideModal}
                    packageId={this.state.data.idPackage}
                />
            </div>
        )
    }

}

const mapStateToProps = state => ({
    user: state.user.user[0],
    userLoading: state.user.loading
})
  
export default connect(mapStateToProps, { setMessage, route })(Dashboard)