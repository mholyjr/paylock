import React, { Component } from 'react'
import { LoginForm } from '../forms'
import { auth } from '../../firebase'
import firebase from 'firebase'
import ReactGA from 'react-ga'

class Login extends Component {

    constructor(props) {
        super(props)
        this.state = ({
            loading: false,
            data: {
                email: '',
                password: ''
            }
        })
    }

    componentDidMount() {
        ReactGA.pageview(window.location.pathname + window.location.search)
    }

    onChangeInput = e => this.setState({
        data: { ...this.state.data, [e.target.name]: e.target.value }
    })

    beforeSubmit = () => {

        if(this.state.data.email !== '' && this.state.data.password) {
            this.submit()
        } else if( this.state.data.email === '') {
            alert('Email is empty')
        } else if(this.state.data.password === '') {
            alert('Password')
        }

    }

    submit = () => {

        this.setState({
            loading: true
        })

        const { history } = this.props

        auth.doSignInWithEmailAndPassword(this.state.data.email, this.state.data.password)
        .then(() => {
            
            firebase.auth().onAuthStateChanged(function(user) {
                if (user) {
                    history.push(`${process.env.PUBLIC_URL}/dashboard`)
                } else {
                    // No user is signed in.
                }
            })

            ReactGA.event({
                category: 'user',
                action: 'login'
            });

        })
        .catch(error => {
            this.setState({
                loading: false
            })

            alert(JSON.stringify(error))
        });
        
    }

    render() {
        return (
            <div className='login-container'>
                <div className='login-form-container'>
                    <div className='logo-container'>
                        <p className='logo'>Paylock</p>
                    </div>
                    <LoginForm 
                        onChange={this.onChangeInput}
                        loading={this.state.loading}
                        onSubmit={this.beforeSubmit}
                    />
                    <p>You don't have a account yet? <a href='/signup'>Please Sign up here.</a></p>
                </div>
            </div>
        )
    }

}

export default Login