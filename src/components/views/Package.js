import React, { Component } from 'react'
import { getPackage, getPackageFiles, deletePackage } from '../../api'
import { connect } from 'react-redux'
import { route } from '../../actions/routeActions'
import { setMessage } from '../../actions/messageActions'
import { PackageContent, PackageInfo } from '../containers'
import { DeletePackageButton, SharePackageButton } from '../buttons'
import { ShareModal, AreYouSureDelete } from '../modals'

class Package extends Component {

    constructor(props) {
        super(props)
        this.state = ({
            data: {},
            files: [],
            route: 'Package details',
            openModal: false,
            areYouSure: false
        })
    }

    deletePackageHandler = (id) => {

        const { history } = this.props

        deletePackage(id)
        .then((res) => {
            this.props.setMessage(res.type, res.text)
            history.push(`${process.env.PUBLIC_URL}/packages`)
        })
    }

    componentDidMount() {

        const { id } = this.props.match.params

        getPackage(id)
        .then((data) => {

            this.props.route(this.state.route + ' | ' + data.data[0].title)

            this.setState({
                data: data.data[0]
            })
        })

        getPackageFiles(id)
        .then((data) => {
            this.setState({
                files: data.data
            })
        })
    }

    openModal = () => {
        this.setState({
            openModal: true
        })
    }

    areYouSureHanlder = () => {
        this.setState({
            areYouSure: true
        })
    }

    hideModal = () => {
        this.setState({
            openModal: false,
            areYouSure: false
        })
    }

    render(){

        const { id } = this.props.match.params

        return (
            <div className='inner-container'>
                <div className='row'>
                    <div className='col-sm-8 no-padding-l'>
                        <PackageContent files={this.state.files} />
                    </div>

                    <div className='col-sm-4 no-padding-r'>
                        <PackageInfo 
                            pkg={this.state.data}
                        />
                        <SharePackageButton 
                            openModal={this.openModal}
                            id={id}
                        />
                        <DeletePackageButton 
                            openModal={this.areYouSureHanlder}
                            id={id}
                        />
                    </div>
                </div>

                <ShareModal 
                    isOpen={this.state.openModal}
                    message='Share your package'
                    hideModal={this.hideModal}
                    packageId={id}
                    pkgMessage={this.state.data.message}
                />

                <AreYouSureDelete 
                    isOpen={this.state.areYouSure}
                    message='Are you sure?'
                    hideModal={this.hideModal}
                    deletePkg={this.deletePackageHandler}
                    id={id}
                />
            </div>
        )
    }

}

const mapStateToProps = state => ({
    
})
  
export default connect(mapStateToProps, { route, setMessage })(Package)