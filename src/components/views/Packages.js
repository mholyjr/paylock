import React, { Component } from 'react'
import { api, packagesLimit } from '../../config'
import { getPackages, getPackagesFilter, deletePackage } from '../../api'
import { connect } from 'react-redux'
import { route } from '../../actions/routeActions'
import { setMessage } from '../../actions/messageActions'
import { PackagesHead, PackagesItem } from '../containers'
import { Button, Icon, Select } from 'semantic-ui-react'
import { AreYouSureDelete } from '../modals'

class Packages extends Component {

    constructor(props) {
        super(props)
        this.state = ({
            packages: [],
            route: 'My packages',
            limit: 1,
            lastPage: '',
            numOfPackages: 0,
            filterBy: '',
            areYouSure: false,
            selectedToDelete: '',
            filter: [
                {key: 1, text: 'All', value: 'all'},
                {key: 2, text: 'Active', value: 'active'},
                {key: 3, text: 'Expired', value: 'expired'},
            ],
            filterDownloads: [
                {key: 1, text: 'All', value: 'all'},
                {key: 2, text: 'No downloads', value: 'nodownload'},
                {key: 3, text: 'Was downloaded', value: 'downloaded'},
            ]
        })
    }

    componentDidMount() {
        this.getPackagesHandler()

        this.props.route(this.state.route)
    }

    deletePackageHandler = () => {

        const { history } = this.props

        deletePackage(this.state.selectedToDelete)
        .then((res) => {
            this.props.setMessage(res.type, res.text)
            this.getPackagesHandler()
            this.hideModal()
        })

    }

    areYouSureHanlder = (id) => {
        this.setState({
            areYouSure: true,
            selectedToDelete: id
        })
    }

    renderPackages = () => {
        if(this.state.packages.length !== 0) {
            return (
                this.state.packages.map((item) => {
                    return (
                        <PackagesItem item={item} deletePkg={this.deletePackageHandler} beforeDelete={this.areYouSureHanlder} />
                    )
                })
            )
        } else {
            return (
                <p>No packages to show</p>
            )
        }
    }

    nextPage = () => {
        this.setState({
            limit: this.state.limit + 1
        }, () => {
            if(this.state.filterBy === '' || this.state.filterBy === 'all') {
                this.getPackagesHandler()
            } else {
                this.filterHander(this.state.filterBy)
            }
            
        })
    }

    prevPage = () => {
        this.setState({
            limit: this.state.limit > 1 ? this.state.limit - 1 : 1
        }, () => {
            if(this.state.filterBy === '' || this.state.filterBy === 'all') {
                this.getPackagesHandler()
            } else {
                this.filterHander(this.state.filterBy)
            }
            
        })
    }

    getPackagesHandler = () => {
        getPackages(this.props.user.uid, this.state.limit)
        .then((res) => {
            this.setState({
                packages: res.data,
                numOfPackages: Number(res.count),
                lastPage: Math.ceil(Number(res.count) / packagesLimit)
            })
        })
    }

    filterHander = (by) => {
        getPackagesFilter(this.props.user.uid, this.state.limit, by)
        .then((res) => {
            this.setState({
                packages: res.data,
                numOfPackages: Number(res.count),
                lastPage: Math.ceil(Number(res.count) / packagesLimit)
            })
        })
    }

    onChangeSelect = (event, data) => {
        this.setState({
            limit: 1,
            filterBy: data.value
        }, () => {
            this.filterHander(data.value)
        })
    }

    hideModal = () => {
        this.setState({
            areYouSure: false
        })
    }

    render() {
        return (
            <div className='inner-container'>
                <div className='list-controls row'>
                    <div className='controls-left col-sm no-padding-l'>
                        <Select placeholder='Filter by state' options={this.state.filter} onChange={this.onChangeSelect} />
                        <Select placeholder='Filter by downloads' options={this.state.filterDownloads} onChange={this.onChangeSelect} />
                    </div>
                    <div className='controls-right col-sm no-padding-r'>
                        <div className='pagination-container'>
                            <Button.Group>
                                <Button primary onClick={this.prevPage} disabled={this.state.limit === 1 ? true : false}>
                                    <Icon name='chevron left' />
                                </Button>
                                <Button primary onClick={this.nextPage} disabled={packagesLimit < this.state.numOfPackages && this.state.limit < this.state.lastPage ? false : true}>
                                    <Icon name='chevron right' />
                                </Button>
                            </Button.Group>
                        </div>
                    </div>
                </div>
                
                <div className='packages-container shadow fadeInDown animated'>
                    <PackagesHead />
                    {this.renderPackages()}
                </div>

                <AreYouSureDelete 
                    isOpen={this.state.areYouSure}
                    message='Are you sure?'
                    hideModal={this.hideModal}
                    deletePkg={this.deletePackageHandler}
                />
            </div>
        )
    }
}

const mapStateToProps = state => ({
    user: state.user.user[0],
    userLoading: state.user.loading
})
  
export default connect(mapStateToProps, { route, setMessage })(Packages)