import React, { Component } from 'react'
import { SignUpForm } from '../forms'
import { auth } from '../../firebase'
import firebase from 'firebase'
import { createProfile } from '../../api'
import ReactGA from 'react-ga'

class SignUp extends Component {

    constructor(props) {
        super(props)
        this.state = ({
            data: {
                first_name: '',
                last_name: '',
                email: '',
                password: ''
            },
            loading: false
        })
    }

    componentDidMount() {
        ReactGA.pageview(window.location.pathname + window.location.search)
    }

    onChangeInput = e => this.setState({
        data: { ...this.state.data, [e.target.name]: e.target.value }
    })

    beforeSubmit = () => {
        if(this.state.data.first_name !== '' && this.state.data.last_name !== '' && this.state.data.email !== '' && this.state.data.password !== '') {
            this.submit()
        } else if( this.state.data.first_name === '') {
            alert('First name is empty')
        } else if(this.state.data.last_name === '') {
            alert('Last name is empty')
        } else if(this.state.data.email === '') {
            alert('Email is empty')
        } else if(this.state.data.password === '') {
            alert('Password is empty')
        }
    }

    submit = () => {

        this.setState({
            loading: true
        })

        auth.doCreateUserWithEmailAndPassword(this.state.data.email, this.state.data.password)
        .then((user) => {
            firebase.auth().onAuthStateChanged((user) => {
                if(user) {
                    this.setState({
                        data: { ...this.state.data, uid: user.uid, email: this.state.data.email }
                    }, () => {
                        createProfile(this.state.data)
                        user.sendEmailVerification()

                        this.setState({
                            loading: false
                        })
                    })
                } else {
                    // No user is signed in.
                }
            })
        })
        .catch(error => {
            
        });

    }

    render() {
        return (
            <div className='login-container'>
                <div className='login-form-container'>
                    <div className='logo-container'>
                        <p className='logo'>Paylock</p>
                    </div>
                    <SignUpForm 
                        onChange={this.onChangeInput}
                        loading={this.state.loading}
                        onSubmit={this.beforeSubmit}
                    />
                    <p>You already have an account? <a href='/login'>Please login here.</a></p>
                </div>
            </div>
        )
    }

}

export default SignUp