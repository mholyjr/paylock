import React from 'react'
import { Button } from 'semantic-ui-react'
import { wallet } from '../../config/images'

const AccountBalance = () => {
    return (
        <div className='widget-container bg-white shadow'>
            <div className='row'>
                <div className='col-sm-6 widget-img-container'>
                    <img src={wallet} className='widget-img' />
                </div>
                <div className='col-sm-6 widget-details'>
                    <div>
                        <h3>Your Balance</h3>
                        {/*<ul>
                            <li>Unlimited # of files</li>
                            <li>Packages up to 10GB</li>
                            <li>Embeed code</li>
                            <li>And more</li>
                        </ul>*/}
                        <div className='balance'>
                            <p>€0</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default AccountBalance