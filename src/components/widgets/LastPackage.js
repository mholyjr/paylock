import React from 'react'
import { Button } from 'semantic-ui-react'
import { packageIcon } from '../../config/images'

const LastPackage = () => {
    return (
        <div className='widget-container bg-white last-package-widget'>
            <div className='row'>
                <div className='col-sm-5 widget-img-container'>
                    <img src={packageIcon} className='widget-img' />
                </div>
                <div className='col-sm-7 widget-details'>
                    <div>
                        <h3>Last package</h3>
                        <Button primary>See details</Button>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default LastPackage