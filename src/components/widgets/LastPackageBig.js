import React from 'react'
import { Button } from 'semantic-ui-react'
import moment from 'moment'

const LastPackageBig = ({ title, message, created, expires, price, downloaded, id, history }) => {
    return (
        <div className='last-package-big shadow animated fadeInRight'>
            <div className='row lpb-top'>
                <div className='col-sm-3'>
                    <span className='top-row-label'>Created</span>
                    <span className='top-row-value'>{moment(created).format("D/M/Y")}</span>
                </div>
                <div className='col-sm-3'>
                    <span className='top-row-label'>Expires</span>
                    <span className='top-row-value'>{moment(expires).format("D/M/Y")}</span>
                </div>
                <div className='col-sm-3'>
                    <span className='top-row-label'>Downloaded</span>
                    <span className='top-row-value'>{downloaded}</span>
                </div>
                <div className='col-sm-3'>
                    <span className='top-row-label'>Price</span>
                    <span className='top-row-value'>€{price}</span>
                </div>
            </div>
            <div className='row lpb-content'>
                <div className='col-sm-8 clearfix lpb-title-container'>
                    <h2>{title}</h2>
                </div>
                <div className='col-sm-8 lpb-message-container'>
                    <p>{message}</p>
                </div>
                <div className='col-sm-4 lpb-button-container'>
                    <Button primary onClick={() => history.push(`${process.env.PUBLIC_URL}/packages/edit/${id}`)}>See details</Button>
                </div>
            </div>
        </div>
    )
}

export default LastPackageBig