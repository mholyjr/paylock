import React from 'react'
import { Button } from 'semantic-ui-react'
import moment from 'moment'

const LastPackageSmall = ({ title, created, expires, history, id }) => {
    return (
        <div className='last-package-small animated fadeInLeft'>
            <div className='row lps-top'>
                <div className='col-sm-6'>
                    <span className='top-row-label'>Created</span>
                    <span className='top-row-value'>{moment(created).format("D/M/Y")}</span>
                </div>
                <div className='col-sm-6'>
                    <span className='top-row-label'>Expires</span>
                    <span className='top-row-value'>{moment(expires).format("D/M/Y")}</span>
                </div>
            </div>
            <div className='row lps-content'>
                <div className='col-sm-8 lps-title-container'>
                    <h2>{title}</h2>
                </div>
                <div className='col-sm-4 lps-button-container'>
                    <Button primary onClick={() => history.push(`${process.env.PUBLIC_URL}/packages/edit/${id}`)}><i class="fas fa-arrow-right"></i></Button>
                </div>
            </div>
        </div>
    )
}

export default LastPackageSmall