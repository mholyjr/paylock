import React from 'react'
import { Button } from 'semantic-ui-react'
import { rocket } from '../../config/images'

const UpgradeAccount = () => {
    return (
        <div className='widget-container bg-gradient-blue shadow'>
            <div className='row'>
                <div className='col-sm-5 upgrade-img'>
                    <img src={rocket} className='widget-img' />
                </div>
                <div className='col-sm-7 widget-details'>
                    <div>
                        <h3>Unlock features</h3>
                        {/*<ul>
                            <li>Unlimited # of files</li>
                            <li>Packages up to 10GB</li>
                            <li>Embeed code</li>
                            <li>And more</li>
                        </ul>*/}
                        <Button primary>Upgrade account</Button>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default UpgradeAccount