import firebase from 'firebase/app'
import 'firebase/auth'

var config = {
    apiKey: "AIzaSyBUVtxMabxFzc2txNb-UtHt5Kq4iDqW6Kk",
    authDomain: "paylock-ff585.firebaseapp.com",
    databaseURL: "https://paylock-ff585.firebaseio.com",
    projectId: "paylock-ff585",
    storageBucket: "paylock-ff585.appspot.com",
    messagingSenderId: "379479062749"
}

if (!firebase.apps.length) {
    firebase.initializeApp(config);
}

const auth = firebase.auth();

export const authRef = firebase.auth();
export const provider = new firebase.auth.GoogleAuthProvider();

export {
  auth,
}