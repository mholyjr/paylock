import * as types from '../actions/types'

let message = { type: '', text: '', visible: false }

const messageReducer = (state = message, action) => {
    switch (action.type) {
        case types.SET_MESSAGE:
            let newState = Object.assign({}, state, { type: action.msgType, text: action.text, visible: true })
            return newState

        case types.UNSET_MESSAGE:
            newState = Object.assign({}, state, { type: '', text: '', visible: false })
            return newState

        default: 
            return state
    }
}

export default messageReducer