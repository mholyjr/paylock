import * as types from '../actions/types'

let route = { head: '' }

const routeReducer = (state = route, action) => {
    switch (action.type) {
        case types.SET_ROUTE:
            let newState = Object.assign({}, state, { head: action.route })
            return newState

        default: 
            return state
    }
}

export default routeReducer