import * as types from '../actions/types'

let userCredentials = { user: [], loading: true }

const userReducer = (state = userCredentials, action) => {
    switch (action.type) {
        case types.FETCHING_USER:
            let newState = Object.assign({}, state, { user: action.user, loading: true })
            return newState

        case types.USER_FETCHED:
            newState = Object.assign({}, state, { user: action.user, loading: false })
            return newState

        default: 
            return state
    }
}

export default userReducer