import { combineReducers } from 'redux'
import userReducer from './reducers/userReducer'
import messageReducer from './reducers/messageReducer'
import routeReducer from './reducers/routeReducer'

export default combineReducers({
    user: userReducer,
    message: messageReducer,
    route: routeReducer
})